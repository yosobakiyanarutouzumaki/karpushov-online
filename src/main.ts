import './assets/reset.css'
import './assets/style.sass'

import { createApp } from 'vue'
import Header from './Header.vue'
import Main from './Main.vue'

createApp(Header).mount('header')
createApp(Main).mount('main')
